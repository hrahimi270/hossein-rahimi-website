import React, { FC } from "react"
import { ThemeProvider, createGlobalStyle } from "styled-components"
import theme from "./theme"
import "./variables.css"

const GlobalStyle = createGlobalStyle`
  html, body, #___gatsby {
    height: 100%;
  }

  main {
    position: relative;
    overflow: hidden;
  }
`

const Layout: FC = ({ children }) => {
    return (
        <ThemeProvider
            theme={{
                ...theme,
            }}
        >
            <GlobalStyle />
            <main>{children}</main>
        </ThemeProvider>
    )
}

export default Layout
