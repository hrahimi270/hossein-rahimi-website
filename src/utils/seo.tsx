import React, { FC } from "react"
import { Helmet } from "react-helmet"
import { useStaticQuery, graphql } from "gatsby"

type TMeta = {
    name: string
    content: string
}

interface ISEO {
    title: string
    description?: string
    keywords?: string[]
    lang?: string
    meta?: TMeta[]
}

const SEO: FC<ISEO> = ({
    title,
    description,
    keywords,
    lang = "en",
    meta = [],
}) => {
    const { site } = useStaticQuery(
        graphql`
            query {
                site {
                    siteMetadata {
                        description
                        author
                    }
                }
            }
        `
    )

    const finalDescription = description || site.siteMetadata.description

    return (
        <Helmet
            htmlAttributes={{
                lang,
            }}
            title={title}
            meta={[
                {
                    name: `description`,
                    content: finalDescription,
                },
                {
                    name: "keywords",
                    content: keywords ? keywords.join(",") : "",
                },
                {
                    property: `og:description`,
                    content: finalDescription,
                },
                {
                    property: `og:type`,
                    content: `website`,
                },
                {
                    name: `twitter:card`,
                    content: `summary`,
                },
                {
                    name: `twitter:creator`,
                    content: site.siteMetadata.author,
                },
                {
                    name: `twitter:title`,
                    content: title,
                },
                {
                    name: `twitter:description`,
                    content: finalDescription,
                },
            ].concat(meta)}
        />
    )
}

export default SEO
