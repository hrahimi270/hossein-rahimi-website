export default class DynamicChars {
    private el: HTMLElement
    private chars: String
    private frame
    private frameRequest
    private resolve
    public queue

    constructor(el: HTMLElement) {
        this.el = el
        this.chars = "!<>-_\\/[]{}░▒▓—åß∂ƒ©˙∆˚æ≈ç√∫=+*^?#________λ$"
        this.frame
        this.update = this.update.bind(this)
    }

    setText(text) {
        let currentText = this.el.innerText
        let difference = Math.max(currentText.length, text.length)

        return new Promise(resolve => {
            this.resolve = resolve
            this.queue = []

            for (var i = 0; i < difference; i++) {
                const from = currentText[i] || ""
                const to = text[i] || ""
                const start = Math.floor(40 * Math.random())
                const end = start + Math.floor(40 * Math.random())

                this.queue.push({
                    from,
                    to,
                    start,
                    end,
                })
            }

            cancelAnimationFrame(this.frameRequest)
            this.frame = 0
            this.update()
        })
    }

    update() {
        console.log(this)
        const queueLength = this.queue.length
        let output = ""
        let t = 0

        for (var i = 0, e = queueLength; i < e; i++) {
            let { from, to, start, end, char } = this.queue[i]

            this.frame >= end
                ? (t++, (output += to))
                : this.frame >= start
                ? (!char || Math.random() < 0.28) &&
                  (((char = this.randomChar()), (this.queue[i].char = char)),
                  (output += '<span class="dud">'.concat(char, "</span>")))
                : (output += from)
        }

        this.el.innerHTML = output
        if (t === queueLength) {
            this.resolve()
        } else {
            this.frameRequest = requestAnimationFrame(this.update)
            this.frame++
        }
    }

    randomChar() {
        return this.chars[Math.floor(Math.random() * this.chars.length)]
    }
}
