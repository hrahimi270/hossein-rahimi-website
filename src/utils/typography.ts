import Typography from "typography"
const typography = new Typography({
    baseFontSize: "18px",
    baseLineHeight: 1.7,
    bodyFontFamily: ["-apple-system", "Inter", "sans-serif"],
    bodyWeight: 400,
    boldWeight: 600,
    headerFontFamily: ["-apple-system", "Inter", "sans-serif"],
    headerLineHeight: 1.8,
    headerWeight: 800,
    scaleRatio: 3.5,
    overrideStyles: ({ adjustFontSizeTo }, options, styles) => ({
        h6: {
            ...adjustFontSizeTo("14px"),
        },
    }),
})

export default typography
