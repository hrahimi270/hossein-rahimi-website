const theme = {
    animation: {
        transition: "all 500ms ease",
    },
    button: {
        radius: "15px",
        shadow: "",
    },
    colors: {},
    spacing: {
        xxs: "0.5rem",
        xs: "1rem",
        sm: "1.5rem",
        md: "2.5rem",
        lg: "5rem",
        xl: "10rem",
        xxl: "15rem",
    },
}

export default theme
