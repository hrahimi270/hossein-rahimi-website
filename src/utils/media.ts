export const size = {
    sm: "640px",
    smH: "650px",
    xsH: "550px",
    md: "800px",
    lg: "1200px",
    xl: "1440px",
    xxl: "1600px",
}

export const media = {
    sm: `(min-width: ${size.sm})`,
    maxSm: `(max-width: ${size.sm})`,
    md: `(min-width: ${size.md})`,
    lg: `(min-width: ${size.lg})`,
    xl: `(min-width: ${size.xl})`,
    xxl: `(min-width: ${size.xxl})`,
    smH: `(max-height: ${size.smH})`,
    xsH: `(max-height: ${size.xsH})`,
    betweenNormalLaptops: `screen, (min-width: ${size.lg}) and (max-width: ${size.xl})`,
    biggerThanNormal: `(min-width: ${size.xl})`,
}
