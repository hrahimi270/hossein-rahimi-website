type keywordsObj = {
    ["keyword"]: string
}[]

export default function extractSeoKeywords(keywords: keywordsObj) {
    return keywords && keywords.length ? keywords.map(item => item.keyword) : []
}
