import React, { FC } from "react"
import styled, { css } from "styled-components"
import { media } from "../../utils/media"

const TitleST = styled(({ styles, children, ...rest }) => (
    <h2 {...rest}>{children}</h2>
))`
    width: auto;
    margin: 0;
    font-weight: 800;
    font-size: 60px;
    color: #fff;
    text-transform: uppercase;
    user-select: none;

    ${props =>
        props.theme.rtl &&
        css`
            font-family: YekanBakh;
            font-weight: 600;
        `}

    @media ${media.smH} {
        font-size: 40px;
        font-weight: 700;
    }

    ${props => props.styles && props.styles}
`

interface ITitle {
    styles?: string
    [x: string]: any
}

const Title: FC<ITitle> = ({ children, styles, ...rest }) => {
    return <TitleST styles={styles} children={children} {...rest} />
}

export default Title
