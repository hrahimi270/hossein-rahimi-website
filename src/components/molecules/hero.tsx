import React, { useEffect, useState } from "react"
import styled from "styled-components"
import gsap from "gsap"
import DynamicChars from "../../utils/dynamic-char"
import { media } from "../../utils/media"

const HeroSectionST = styled.section`
    height: 100vh;
    position: relative;
    overflow: hidden;
`
const HeroBackgroundST = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
`
const HeroFrontST = styled.div`
    position: relative;
    height: 100%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    padding: 0 6rem;

    .hero_front--paragraph {
        font-weight: 900;
        font-size: 60px;
        text-transform: uppercase;
        color: #000;
        margin: 0;
        overflow: hidden;

        /* default */
        opacity: 0;

        span {
            display: inline-block;
        }

        .whitespace {
            width: 10px;
        }

        &.second {
            font-size: 55px;

            .my-dynamic-info {
                margin-left: 10px;
                color: #333;
            }
        }

        &.third {
            font-size: 50px;
        }
    }

    @media ${media.smH} {
        padding: 0 4rem;

        .hero_front--paragraph {
            font-size: 50px;

            &.second {
                font-size: 45px;
            }

            &.third {
                font-size: 40px;
            }
        }
    }

    @media ${media.xsH} {
        .hero_front--paragraph {
            font-size: 45px;

            &.second {
                font-size: 40px;
            }

            &.third {
                font-size: 35px;
            }
        }
    }
`
const HosseinImageWrapper = styled.div`
    position: absolute;
    right: 6rem;
    display: flex;
    align-items: center;
    justify-content: center;

    img {
        width: 400px;
        position: relative;
        margin: 0;
        z-index: 1;
        opacity: 0;

        @media ${media.smH} {
            width: 350px;
        }

        @media ${media.xsH} {
            width: 300px;
        }
    }

    .picture_wrapper--img-back {
        position: absolute;
        width: 120%;
        height: 0;
        background-color: #000;
    }

    .picture_wrapper--img-front {
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        height: 0;
        background-color: #fff;
        z-index: 2;

        &.revealed {
            bottom: unset;
            top: 0;
        }
    }
`

const hosseinSrc = "/hossein.jpg"
const HeroSection = () => {
    const [isTimelineFinished, setTimelineFinished] = useState(false)

    // Handle splitting and text-reveal animations
    useEffect(() => {
        // splitting
        const Splitting =
            typeof window !== `undefined` ? require("splitting") : null
        const splittingOptions = {}

        Splitting(splittingOptions)

        const heroFront = document.querySelector(".hero .hero_front")
        const paragraphs = heroFront.querySelectorAll(".hero_front--paragraph")
        const chars = heroFront.querySelectorAll(
            ".hero_front--paragraph .word > .char, .whitespace"
        )
        const pictureWrapper = document.querySelector(".picture_wrapper")
        const pictureBack = pictureWrapper.querySelector(
            ".picture_wrapper--img-back"
        )
        const pictureFront = pictureWrapper.querySelector(
            ".picture_wrapper--img-front"
        )
        const pictureImg = pictureWrapper.querySelector("img")

        const welcomeTL = gsap
            .timeline()
            .set(chars, {
                y: "100%",
                opacity: 0,
            })
            .set(paragraphs, {
                opacity: 1,
            })
            .to(chars, {
                y: "0%",
                opacity: 1,
                stagger: 0.014,
                duration: 0.5,
                ease: "Power3.easeOut",
                delay: 1,
            })

        const pictureTL = gsap
            .timeline()
            .to(pictureBack, {
                height: "60%",
                ease: "expo.out",
            })
            .to(pictureFront, {
                height: "100%",
                duration: 0.5,
                ease: "expo.out",
                onComplete: () => pictureFront.classList.add("revealed"),
            })
            .set(pictureImg, {
                opacity: 1,
            })
            .to(
                pictureFront,
                {
                    height: "0%",
                    duration: 0.5,
                    ease: "expo.out",
                },
                "+=0.2"
            )
            .set(pictureFront, {
                opacity: 0,
            })

        const mainTL = gsap.timeline({
            paused: true,
            onComplete: () => setTimelineFinished(true),
        })
        mainTL.add(welcomeTL)
        mainTL.add(pictureTL, "+=0.1")
        mainTL.play()
    }, [])

    // Handle dynamic chars
    useEffect(() => {
        if (isTimelineFinished) {
            const secondParagraph = document.querySelector(
                ".hero_front .second .my-dynamic-info"
            ) as HTMLElement
            const strings = secondParagraph.getAttribute("data-options")
            const arrayStrings = JSON.parse(strings) as String[]
            const dynamicChars = new DynamicChars(secondParagraph)
            let currentStep = 1 //

            function recursiveCharsCall() {
                dynamicChars.setText(arrayStrings[currentStep]).then(() => {
                    setTimeout(recursiveCharsCall, 2500)
                })
                currentStep = (currentStep + 1) % arrayStrings.length
            }
            recursiveCharsCall() // call for firs time
        }
    }, [isTimelineFinished])

    return (
        <HeroSectionST className="hero">
            <HeroBackgroundST className="hero_background" />
            <HeroFrontST className="hero_front">
                <h1 style={{ display: "none" }}>
                    Welcome to Hossein Rahimi personal website.
                </h1>
                <div
                    className="hero_front--paragraph first"
                    data-splitting="chars"
                >
                    Hi.
                </div>
                <div className="hero_front--paragraph second">
                    <span data-splitting="chars">I'm</span>
                    <span
                        className="my-dynamic-info"
                        data-splitting="chars"
                        data-options='["Hossein Rahimi", "Front-End Developer", "WebGL Developer", "Movie Lover", "Gamer Fan"]'
                    >
                        Hossein Rahimi
                    </span>
                    <span data-splitting="chars">.</span>
                </div>
                <div
                    className="hero_front--paragraph third"
                    data-splitting="chars"
                >
                    Welcome to my Website.
                </div>
                <HosseinImageWrapper className="picture_wrapper">
                    <div className="picture_wrapper--img-back" />
                    <img src={hosseinSrc} alt="Hossein Rahimi" />
                    <div className="picture_wrapper--img-front" />
                </HosseinImageWrapper>
            </HeroFrontST>
        </HeroSectionST>
    )
}

export default HeroSection
