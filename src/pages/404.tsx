import React, { FC } from "react"
import styled from "styled-components"
import { AiOutlineArrowLeft } from "react-icons/ai"
import Layout from "../utils/layout"
import SEO from "../utils/seo"
import Title from "../components/atoms/title"

const CustomSectionST = styled.section`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

    p {
        color: #fff;
        margin-top: 0.5rem;
    }

    a {
        color: #fff;
        text-decoration: none;
        display: flex;
        align-items: center;

        svg {
            margin-right: 0.5rem;
        }
    }
`

const NotFoundPage: FC = () => {
    return (
        <Layout>
            <SEO title="404 - Page not found" />
            <CustomSectionST>
                <Title>Page not found!</Title>
                <p>You just hit a route that doesn't exist... the sadness ):</p>

                <a>
                    <AiOutlineArrowLeft />
                    <span>Back To Home</span>
                </a>
            </CustomSectionST>
        </Layout>
    )
}

export default NotFoundPage
