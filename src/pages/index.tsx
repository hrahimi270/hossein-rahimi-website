import React, { FC } from "react"
import Layout from "../utils/layout"
import SEO from "../utils/seo"
import HeroSection from "../components/molecules/hero"

const IndexPage: FC = () => {
    return (
        <Layout>
            <SEO title="Hossein Rahimi Personal Website" />
            <HeroSection />
        </Layout>
    )
}

export default IndexPage
