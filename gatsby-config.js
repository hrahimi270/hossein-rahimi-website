require("dotenv").config({
    path: `.env`,
})

module.exports = {
    siteMetadata: {
        title: `Hossein Rahimi Personal Website`,
        description: `You can know me better and get familiar with my career. Here, I try to put useful data about my profession.`,
        author: `hrahimi270@gmail.com`,
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        `gatsby-plugin-styled-components`,
        {
            resolve: "gatsby-plugin-react-svg",
            options: {
                rule: {
                    include: /\.svg$/,
                },
            },
        },
        {
            resolve: `gatsby-plugin-typography`,
            options: {
                pathToConfigModule: `src/utils/typography`,
            },
        },
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `gatsby-starter-default`,
                short_name: `starter`,
                start_url: `/`,
                display: `minimal-ui`,
                icon: `src/assets/favicon.jpg`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/assets`,
            },
        },
    ],
}
